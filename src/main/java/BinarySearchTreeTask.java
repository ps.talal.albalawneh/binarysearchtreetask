public class BinarySearchTreeTask {
    public static void main(String[] args){
        BinarySearchTree tree = new BinarySearchTree();

        tree.accept(5);
        tree.accept(4);
        tree.accept(6);
        tree.accept(7);

        System.out.println(tree.depth(6));
        System.out.println(tree.getTreeDepth());

    }
}
