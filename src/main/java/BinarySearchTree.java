public class BinarySearchTree {
    private Node root;
    private int treeDepth;

    public BinarySearchTree() {
        this.root = null;
        this.treeDepth = 0;
    }

    public BinarySearchTree(int data){
        this.root = new Node(data);
        this.treeDepth = 1;
    }

    // add new node to the tree,
    // it will invoke recAccept() to specify the position for new node.
    public boolean accept(int data){
        if(depth(data) > 0){
            System.out.println("Node already exist.");
            return false;
        }else {
            this.root = recAccept(this.root, data);
            System.out.println("Node added.");
            return true;
        }
    }

    // add new node recursively,
    // the smaller value nodes add on the left and greater one add to the right
    private Node recAccept(Node root, int data){
        if(root == null){
            root = new Node(data);
            return root;
        }
        if(root.getData() > data){
            root.left = recAccept(root.left, data);
        }
        if(root.getData() < data){
            root.right = recAccept(root.right, data);
        }
        return root;
    }

    // return the depth for a node
    public int depth(int key){
        int depth = 1;
        boolean isFound = false;
        Node head = this.root;
        while (head != null && head.getData() != key){
            if(key > head.getData()){
                ++depth;
                head = head.right;
            } else if(key < head.getData()){
                ++depth;
                head = head.left;
            }
        }
        isFound = (head != null && key == head.getData());
        this.treeDepth = Math.max(treeDepth, depth);
        return isFound ? depth : 0;
    }

    // return the tree depth
    public int getTreeDepth(){
        return treeDepth;
    }

    // print the tree
    public void printInOrder(){
        recPrintInOrder(root);
    }

    private void recPrintInOrder(Node root) {
        if (root != null){
            recPrintInOrder(root.left);
            System.out.print(root.getData() + " ");
            recPrintInOrder(root.right);
        }
    }
}
